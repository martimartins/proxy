## Proxy server

No exemplo a baixo, o HTTP proxy server utiliza socket protocol
para receber requests HTTP raw,
pelo client.

Este caso pratico ira mostrar quanta informação
uma proxy consegue ter do cliente.

![forward_proxy](https://www.ibm.com/docs/en/ssw_ibm_i_72/rzaie/rzal8502.gif)